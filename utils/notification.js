const notification = {
  success(title, duration, icon) {
    title = title || "成功";
    icon = icon || "success";
    if (title.length > 7) {
      //提示语长度大于7
      icon = "none";
    }
    wx.showToast({
      title: title || "成功",
      duration: duration || 2000,
      icon: icon,
      mask:true
    })
  },
  /**
   * icon 绝对路径
   */
  error(title, duration, icon) {
    let toastobj={
      title: title || "系统错误",
      duration: duration || 2000,
      image: icon || "/static/warning.png",
      icon:"none",
      mask: true
    }
    if (title.length > 7) {
      //提示语长度大于7
      toastobj.icon = "none";
      delete toastobj.image;
    }
    wx.showToast(toastobj)
  }

}

module.exports = notification;