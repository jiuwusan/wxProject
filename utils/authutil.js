const authutil = {
  /**
   * 用于验证用户的信息
   */
  checkAuth(repData) {
    console.log("正在验证用户身份", repData.statusCode);
  },
  setWxUserinfo(wxUserInfo) {
    if (wxUserInfo) {
      wx.setStorageSync("wxUserInfo", wxUserInfo);
    } else {
      wx.getSetting({
        success(res) {
          if (res.authSetting['scope.userInfo']) {
            // 已经授权，可以直接调用 getUserInfo 获取头像昵称
            wx.getUserInfo({
              success: function(res) {
                console.log("获取微信用户信息成功", res.userInfo);
                wx.setStorageSync("wxUserInfo", res.userInfo);
              }
            })
          }
        }
      })
    }
  },
  getWxUserinfo() {
    return wx.getStorageSync("wxUserInfo");
  }
}
module.exports = authutil;