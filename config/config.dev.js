/**
 * 服务器配置
 */
const server = {
  hostname: "http://127.0.0.1:9532",
  apiPrefix: "http://127.0.0.1:9532",
  filePrefix: "http://127.0.0.1:9532",
  miniType: "?servertype=wx"
}

//上传接口
const upload = {
  file: server.hostname + "/common/upload/img" + server.miniType,
  img: server.hostname + "/common/upload/img" + server.miniType
};

module.exports = {
  server: server,
  upload: upload
}