/**
 * 服务器配置
 */
const server = {
  hostname: "https://api.jiuwusan.cn",
  apiPrefix: "https://api.jiuwusan.cn",
  filePrefix: "https://api.jiuwusan.cn",
  miniType: "?servertype=wx"
}

//上传接口
const upload = {
  file: server.hostname + "/common/upload/img" + server.miniType,
  img: server.hostname + "/common/upload/img" + server.miniType
}

module.exports = {
  server: server,
  upload: upload
}