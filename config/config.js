/**
 * 环境配置
 */
const env = "dev";

const config = require(`./config.${env}.js`);

module.exports = config;