const authutil = require('./utils/authutil.js');
App({
  onLaunch: function() {
    //获取用户信息，并放入缓存
    authutil.setWxUserinfo();
  },
  globalData: {
    userInfo: null
  }
})