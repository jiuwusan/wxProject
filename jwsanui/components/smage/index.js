const config = require('../../../config/config.js');
Component({
  externalClasses: ['item-class', 'box-class'],
  /**
   * 组件的属性列表
   */
  properties: {
    src: { // 属性名
      type: String,
      optionalTypes: [Array],
      value: ""
    },
    preview: { // 属性名
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    srcs: []
  },
  observers: {
    //监听配置文件
    'src': function(value) {
      const {
        filePrefix,
        miniType
      } = config.server;
      let srcs = [];
      if (value) {
        if (Object.prototype.toString.call(value) == '[object Array]') {
          srcs = value;
        } else {
          value = value.replace(/\ /, "");
          if (value == "") {
            srcs = [];
          } else {
            srcs = value.split(",");
          }
        }
      }

      for (var i = 0; i < srcs.length; i++) {
        srcs[i] = filePrefix + srcs[i] + miniType
      }

      this.setData({
        srcs
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    previewImage: function(e) {
      let index = e.currentTarget.dataset.index;
      let urls = this.data.srcs;
      wx.previewImage({
        current: index,
        urls: urls
      })
    }
  }
})