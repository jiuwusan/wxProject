let countDownInterval = null;
Component({
  externalClasses: ['count-class'],
  /**
   * 组件的属性列表
   */
  properties: {
    count: { // 图形验证码的有效时间
      type: Number,
      value: 60
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    countdown: 60
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      this.beginCountDwon();
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      this.clearCountDwon();
    },
  },
  observers: {
    'count': function(value) {
      this.setData({
        countdown: value
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    beginCountDwon() {
      let that = this;
      that.clearCountDwon();
      countDownInterval = setInterval(() => {
        let {
          countdown,
          count
        } = that.data;
        countdown = countdown * 1 - 1;
        that.setData({
          countdown: countdown < 10 ? ("0" + (countdown + "")) : countdown
        })
        if (countdown < 1) {
          that.clearCountDwon();
          that.bindchange({
            count
          })
          that.setData({
            countdown: count
          })
        }
      }, 1000)
    },
    clearCountDwon() {
      if (countDownInterval) {
        clearInterval(countDownInterval);
      }
    },
    bindchange: function(object) {
      console.log("计时结束")
      this.triggerEvent('over', object);
    },
  }
})