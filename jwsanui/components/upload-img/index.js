const config = require('../../../config/config.js');
const uploadApi = require('../../../api/uploadApi.js');
const regeneratorRuntime = require('../../../module/runtime.js')
const {
  filePrefix
} = config.server;
Component({
  //该主键为表单组件
  behaviors: ['wx://form-field'],
  //定义外部class名字
  externalClasses: ['item-class', 'box-class', 'row-class', 'icon-class'],
  /**
   * 组件的属性列表
   */
  properties: {
    initvalue: { // 初始值
      type: String,
      optionalTypes: [Array],
      value: ""
    },
    max: { // 最大张数
      type: Number,
      value: 1
    },
    source: { // 来源 1 相册，2相机，-1，所有
      type: Number,
      value: -1
    },
    del: { // 是否显示删除按钮
      type: Boolean,
      value: true
    },
    icon: { // 自定义上传icon
      type: String,
      value: null
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    icondef: "data:image/svg+xml;base64,PHN2ZyB0PSIxNTYyMzEyOTk0MDI0IiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjEyOTIiIHdpZHRoPSIyMDAiIGhlaWdodD0iMjAwIj48cGF0aCBkPSJNODA1LjMgMjcyYzI5LjQgMCA1NC42IDEwLjQgNzUuNCAzMS4zIDIwLjkgMjAuOSAzMS4zIDQ1LjkgMzEuMyA3NS40Vjc1MmMwIDI5LjQtMTAuNCA1NC42LTMxLjMgNzUuNC0yMC45IDIwLjktNDUuOSAzMS4zLTc1LjQgMzEuM0gyMTguN2MtMjkuNCAwLTU0LjYtMTAuNC03NS40LTMxLjNTMTEyIDc4MS41IDExMiA3NTJWMzc4LjZjMC0yOS40IDEwLjQtNTQuNiAzMS4zLTc1LjRzNDUuOS0zMS4zIDc1LjQtMzEuM2g5My40bDIxLjItNTYuNmM1LjItMTMuNiAxNS0yNS40IDI5LTM1LjIgMTQtOS44IDI4LjQtMTQuOCA0My4xLTE0LjhoMjEzLjNjMTQuOCAwIDI5LjEgNC45IDQzLjEgMTQuOCAxNCA5LjggMjMuNyAyMS42IDI5IDM1LjJsMjEuMiA1Ni42aDkzLjV2MC4xek0zODAuMiA2OTcuMWMzNi41IDM2LjUgODAuNSA1NC44IDEzMS44IDU0LjggNTEuNCAwIDk1LjMtMTguMyAxMzEuOC01NC44IDM2LjUtMzYuNSA1NC44LTgwLjUgNTQuOC0xMzEuOCAwLTUxLjQtMTguMy05NS4zLTU0LjgtMTMxLjgtMzYuNS0zNi41LTgwLjUtNTQuOC0xMzEuOC01NC44cy05NS4zIDE4LjMtMTMxLjggNTQuOC01NC44IDgwLjUtNTQuOCAxMzEuOGMwIDUxLjMgMTguMiA5NS4zIDU0LjggMTMxLjh6IG00Ny4xLTIxNi42YzIzLjUtMjMuNSA1MS43LTM1LjIgODQuNy0zNS4yczYxLjMgMTEuNyA4NC43IDM1LjJjMjMuNSAyMy41IDM1LjIgNTEuNyAzNS4yIDg0LjdzLTExLjcgNjEuMy0zNS4yIDg0LjdjLTIzLjUgMjMuNS01MS43IDM1LjItODQuNyAzNS4ycy02MS4zLTExLjctODQuNy0zNS4yYy0yMy41LTIzLjUtMzUuMi01MS43LTM1LjItODQuN3MxMS43LTYxLjIgMzUuMi04NC43eiIgcC1pZD0iMTI5MyIgZmlsbD0iI2JmYmZiZiI+PC9wYXRoPjwvc3ZnPg==",
    delicon: "data:image/svg+xml;base64,PHN2ZyB0PSIxNTgyMjgwMTExMzAzIiBjbGFzcz0iaWNvbiIgdmlld0JveD0iMCAwIDEwMjQgMTAyNCIgdmVyc2lvbj0iMS4xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHAtaWQ9IjIyODYiIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiI+PHBhdGggZD0iTTUxNS4zNTY0NDQgOTE0LjY1OTU1NkE0MDQuODc4MjIyIDQwNC44NzgyMjIgMCAwIDEgMzU3LjcxNzMzMyAxMzcuMzg2NjY3YTQwNC43MDc1NTYgNDA0LjcwNzU1NiAwIDAgMSA1MjEuNjE0MjIzIDIxOC44NTE1NTUgNDA0Ljg3ODIyMiA0MDQuODc4MjIyIDAgMCAxLTIwNi4zMzYgNTI2Ljc5MTExMSA0MDMuMDAwODg5IDQwMy4wMDA4ODkgMCAwIDEtMTU3LjYzOTExMiAzMS42MzAyMjN6IG0wLTc0Ni4zODIyMjNjLTE4OC41ODY2NjcgMC0zNDEuNTYwODg5IDE1My43MTM3NzgtMzQxLjU2MDg4OCAzNDEuNTA0czE1Mi45MTczMzMgMzQxLjUwNCAzNDEuNTYwODg4IDM0MS41MDRjMTg4LjUyOTc3OCAwIDM0MS41MDQtMTUyLjkxNzMzMyAzNDEuNTA0LTM0MS41MDRzLTE1My43NzA2NjctMzQxLjUwNC0zNDEuNTA0LTM0MS41MDR6IG0wIDAiIHAtaWQ9IjIyODciIGZpbGw9IiNkODFlMDYiPjwvcGF0aD48cGF0aCBkPSJNMzY0LjAzMiA2OTIuODQ5Nzc4YTMxLjc0NCAzMS43NDQgMCAwIDEtMjIuMjQzNTU2LTUzLjkzMDY2N2wzMDEuOTY2MjIzLTMwMS45NjYyMjJhMzEuODU3Nzc4IDMxLjg1Nzc3OCAwIDEgMSA0NS4xMTI4ODkgNDUuMjI2NjY3TDM4Ni45NTgyMjIgNjgzLjIzNTU1NmEzMS42MzAyMjIgMzEuNjMwMjIyIDAgMCAxLTIyLjkyNjIyMiA5LjYxNDIyMnogbTAgMCIgcC1pZD0iMjI4OCIgZmlsbD0iI2Q4MWUwNiI+PC9wYXRoPjxwYXRoIGQ9Ik02NjUuODg0NDQ0IDY5Mi44NDk3NzhhMzEuNTczMzMzIDMxLjU3MzMzMyAwIDAgMS0yMi4xMjk3NzctOS41NTczMzRMMzQxLjc4ODQ0NCAzODEuNDRhMzEuOTcxNTU2IDMxLjk3MTU1NiAwIDAgMSA0NS4xNjk3NzgtNDUuMTY5Nzc4bDMwMS4xMTI4ODkgMzAyLjY0ODg4OWEzMS44NTc3NzggMzEuODU3Nzc4IDAgMCAxLTIyLjE4NjY2NyA1My45MzA2Njd6IG0wIDAiIHAtaWQ9IjIyODkiIGZpbGw9IiNkODFlMDYiPjwvcGF0aD48L3N2Zz4=",
    value: [],
    temps: [],
    paths: []
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行

    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  observers: {
    //监听配置文件
    'initvalue': function(value) {
      console.log("触发了==", value);
      let that = this;

      let paths = [];
      if (value) {
        if (Object.prototype.toString.call(value) == '[object Array]') {
          paths = value;
        } else {
          value = String(value);
          if (value != "") {
            paths = value.split(",");
          }
        }
      }
      let setvalue = paths;
      for (var i = 0; i < paths.length; i++) {
        paths[i] = that.fttpath(paths[i]);
      }

      this.setData({
        paths,
        value: setvalue || []
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    fttpath(path) {
      path = path || "";
      if (path.indexOf("http") > -1) {
        return path;
      }
      return filePrefix + path;
    },
    chooseImage: function() {
      let that = this;
      //计算可选张数
      let {
        max,
        source,
        temps,
        paths
      } = that.data;
      let count = max - paths.length - temps.length;
      let sourceType = ['album', 'camera'];
      if (source == 1) {
        sourceType = ['album'];
      } else if (source == 2) {
        sourceType = ['camera'];
      }
      wx.chooseImage({
        count: count,
        sizeType: ['original', 'compressed'],
        sourceType: sourceType,
        success(res) {
          // tempFilePath可以作为img标签的src属性显示图片
          const tempFilePaths = res.tempFilePaths;
          // temps = temps.concat(tempFilePaths);
          // that.setData({
          //   temps
          // })
          //开始上传
          that.uploadFiles(tempFilePaths);
        }
      })
    },
    async uploadFiles(array) {
      let that = this;
      //防止深克隆
      let comdata = JSON.parse(JSON.stringify(that.data))
      let {
        temps,
        paths,
        value
      } = comdata;
      value = value || [];
      wx.showLoading({
        title: '上传中',
        mask: true
      })
      console.log("上传前", {
        temps,
        paths,
        value
      })
      for (var i = 0; i < array.length; i++) {
        let rs = await uploadApi.uploadPic(array[i]);
        console.log("图片上传结果", rs);
        if (rs.path != null) {
          paths.push(that.fttpath(rs.path));
          value.push(rs.path);
          //向父组件传值
          that.bindchange({
            value
          });
        }
      }
      console.log("上传后", {
        temps,
        paths,
        value
      })
      that.setData({
        // temps: [],
        paths,
        value
      })
      wx.hideLoading();
    },
    /**
     * 删除图片
     */
    delImg: function(e) {
      let that = this;
      let index = e.currentTarget.dataset.index;
      let thatdata = JSON.parse(JSON.stringify(that.data));
      let {
        value,
        paths
      } = thatdata;
      //移除元素
      value.splice(index, 1);
      paths.splice(index, 1);
      that.setData({
        paths,
        value
      })
      //向父组件传值
      that.bindchange({
        value
      });
    },
    /**
     * 放大图片
     */
    previewImage: function(e) {
      let index = e.currentTarget.dataset.index;
      let urls = this.data.paths;
      wx.previewImage({
        current: index,
        urls: urls
      })
    },
    /**
     * 向外部提供change回调
     */
    bindchange: function(object) {
      this.triggerEvent('change', object);
    }
  }
})