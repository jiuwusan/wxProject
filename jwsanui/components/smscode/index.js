//引入通用上传模块
const authApi = require('../../../api/authApi.js');
let countDownInterval = null;
Component({
  externalClasses: ['code-class'],
  /**
   * 组件的属性列表
   */
  properties: {
    phone: { // 手机号
      type: String,
      value: ""
    },
    vercode: { // 图形验证码
      type: String,
      value: ""
    },
    verkey: { // 图形验证码的key值
      type: String,
      value: ""
    },
    count: { // 图形验证码的有效时间
      type: Number,
      value: 90
    },
    placeholder: { // 获取的提示语言
      type: String,
      value: "获取验证码"
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    codeStatus: true,
    countdown: 90,
    codekey: null
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      this.clearCountDwon();
    },
  },
  observers: {
    'count': function(value) {
      this.setData({
        countdown: value
      })
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    getsmscode() {
      let that = this;
      let {
        phone,
        vercode,
        verkey,
        codeStatus
      } = that.data;
      if (!codeStatus) {
        wx.showToast({
          title: '短信验证码正在发送',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      phone = phone || null;
      vercode = vercode || null;
      verkey = verkey || null;
      if (phone == null) {
        wx.showToast({
          title: '手机号不能为空',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (vercode == null) {
        wx.showToast({
          title: '图形验证码不能为空',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      if (verkey == null) {
        wx.showToast({
          title: '图形验证码不能为空',
          icon: 'none',
          duration: 2000
        })
        return false;
      }
      authApi.getSmsCode({
        phone,
        vercode,
        verkey
      }).then(res => {
        if (res.code == 200) {
          wx.showToast({
            title: "发送中",
            icon: 'none',
            duration: 2000
          })
          that.setData({
            codekey: res.data.key,
            codeStatus: false
          })
          that.beginCountDwon();
          that.bindchange({
            "key": res.data.key
          })
        } else {
          wx.showToast({
            title: rs.msg,
            icon: 'none',
            duration: 2000
          })
        }
      }).catch(err => {
        wx.showToast({
          title: '短信验证码获取失败',
          icon: 'none',
          duration: 2000
        })
      })
    },
    bindchange: function(object) {
      this.triggerEvent('change', object);
    },
    beginCountDwon() {
      let that = this;
      countDownInterval = setInterval(() => {
        let {
          countdown,
          count
        } = that.data;
        countdown = countdown - 1;
        that.setData({
          countdown
        })
        if (countdown < 1) {
          that.clearCountDwon();
          that.setData({
            countdown: count,
            codeStatus: true
          })
        }
      }, 1000)
    },
    clearCountDwon(){
      if (countDownInterval){
        clearInterval(countDownInterval);
      }
    }
  }
})