//引入通用上传模块
const authApi = require('../../../api/authApi.js');
Component({
  externalClasses: ['code-class'],
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    codesrc: null,
    codekey: null
  },
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行
      this.refreshcode();
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
    },
  },
  observers: {},
  /**
   * 组件的方法列表
   */
  methods: {
    refreshcode() {
      let that = this;
      authApi.getImgCode({}).then(res => {
        if (res.code == 200) {
          that.setData({
            codesrc: res.data.code,
            codekey: res.data.key
          })
          that.bindchange({
            "key": res.data.key
          })
        } else {
          wx.showToast({
            title: '图形验证码获取失败',
            icon: 'none',
            duration: 2000
          })
        }
      }).catch(err => {
        wx.showToast({
          title: '图形验证码获取失败',
          icon: 'none',
          duration: 2000
        })
      })
    },
    bindchange: function(object) {
      this.triggerEvent('change', object);
    }
  }
})