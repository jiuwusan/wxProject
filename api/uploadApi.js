const config = require('../config/config.js');
/**
 *获取用户openid 
 */
const uploadPic = (filePath, url) => {
  url = url || config.upload.file;
  //构建参数
  return new Promise((resolve, reject) => {
    wx.uploadFile({
      url: url, //接口地址
      filePath: filePath,
      name: 'file',
      formData: {},
      success(res) {
        console.log(res);
        try {
          //返回图片路径,对返回的结果进行处理
          let data = JSON.parse(res.data);
          if (data.code == 200 || data.code == 2001) {
            resolve({
              "path": data.path
            });
          } else {
            resolve({
              "path": null
            });
          }
        } catch (err) {
          resolve({
            "path": null
          });
        }
      },
      fail(err) {
        wx.showToast({
          title: '文件上传失败',
          icon: 'none'
        })
        reject({
          "path": null
        });
      }
    })

  })
}

module.exports = {
  uploadPic: uploadPic
}