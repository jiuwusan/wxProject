const request = require('../utils/request.js');

/**
 *获取用户openid 
 */
const getWxOpenid = () => {
  //构建参数
  return new Promise((resolve, reject) => {
    request("/weixin/wxxcxfangguan/getCommittees").then(res => {
      resolve(res);
    }).catch(err => {
      reject(err);
    })
  })
}

module.exports = {
  getWxOpenid: getWxOpenid
}