const APIFunction = require('./APIFunction.js');
const api = {
  loginPhone: 'POST /api/auth/loginPhone',
  wxlogin: 'POST /api/auth/wxlogin',
  getSmsCode: 'POST /common/sms/sendCode',
  getImgCode: '/auth/getImgCode',
  genJwtToken: '/auth/genJwtToken',
  valiLogin: 'POST /api/auth/login',
  queryUserInfo: '/api/auth/queryUserInfo'//get请求
}
const API = APIFunction(api);

module.exports = API;