const regeneratorRuntime = require('../../module/runtime.js')
const authApi = require('../../api/authApi.js');
const notification = require('../../utils/notification.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    uploadimgs: [],
    option: {
      url: "/common/upload/img",
      host: "https://jiuwusan.cn",
      miniType: "",
      warming: {
        txt: "",
      },
      addIcon: 2
    },
    phone: "15330368781",
    vercode: "",
    verkey: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getToken();
    notification.error("数据异常");
    // wx.showToast({
    //   title:"数据异常",
    //   duration:2000,
    //   image:"../../static/warning.png",
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getValue: function(e) {
    let value = e.detail.value; // 自定义组件触发事件时提供的detail对象
    console.log("value====", value);
  },
  onInput: function(e) {
    console.log("input输入==", e.detail);
  },
  async getToken() {
    let rs = await authApi.genJwtToken();
    console.log("请求结果", rs);
  },
  vercodechange(e) {
    console.log("刷新图形验证码", e)
    this.setData({
      verkey:e.detail.key
    })
  },
  smscodechange(e) {
    console.log("刷新短信验证码", e);
  }
})