const regeneratorRuntime = require('../../../module/runtime.js')
const authApi = require('../../../api/authApi.js');
const notification = require('../../../utils/notification.js');
const authutil = require('../../../utils/authutil.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  getUserInfo(e) {
    if (e.detail.userInfo) {
      authutil.setWxUserinfo(e.detail.userInfo);
      this.wxLogin();
    }else{
      notification.error("取消授权");
    }
  },
  wxLogin() {
    let that = this;
    wx.login({
      success(res) {
        console.log('登录结果==', res);
        if (res.code) {
          //发起网络请求
          that.getOpenid(res.code);
        } else {}
      }
    })
  },
  async getOpenid(code) {
    let rs = await authApi.wxlogin({
      code
    });
    console.log("登录结果", rs);
    if (rs.code == 200) {
      //跳转页面
      wx.reLaunch({
        url: '/pages/index/index',
      })
    }
  },
  toPhoneLogin() {
    wx.navigateTo({
      url: '/pages/auth/loginbyphone/index',
    })
  }
})