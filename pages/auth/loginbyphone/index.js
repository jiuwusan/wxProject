const regeneratorRuntime = require('../../../module/runtime.js')
const authApi = require('../../../api/authApi.js');
const notification = require('../../../utils/notification.js');
const util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    smscodestatus: false,
    codekey: "",
    verkey: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  formSubmit(e) {
    console.log("提交了表单", e);
    let operation = e.detail.target.id;
    if (operation == "login") {
      this.login(e.detail.value)
    } else if (operation == "sendcode") {
      this.sendSmsCode(e.detail.value)
    }
  },
  async sendSmsCode(formdata) {
    let that = this;
    let mapreq = {
      phone: "请输入手机号",
      vercode: "请输入图形验证码"
    };
    if (!util.isFormRequired(formdata, mapreq)) {
      return false;
    }
    let rs = await authApi.getSmsCode({
      phone: formdata.phone,
      vercode: formdata.vercode,
      verkey: this.data.verkey
    });
    if (rs.code == 200) {
      notification.success("发送成功");
      that.setData({
        codekey: rs.data.key,
        smscodestatus: true
      })
    } else {
      notification.error(rs.msg);
    }
  },
  async login(formdata) {
    let that = this;
    let mapreq = {
      phone: "请输入手机号",
      smscode: "请输入短信验证码"
    };
    if (!util.isFormRequired(formdata, mapreq)) {
      return false;
    }
    let rs = await authApi.loginPhone({
      phone: formdata.phone,
      smscode: formdata.smscode,
      codekey: this.data.codekey
    });
    if (rs.code == 200) {
      notification.success("登录成功");
      //在这里跳转页面
      wx.reLaunch({
        url: '/pages/index/index',
      })
    } else {
      notification.error(rs.msg);
    }
  },
  countover(e) {
    this.setData({
      smscodestatus: false
    })
  },
  vercodechange(e) {
    console.log("刷新验证码")
    this.setData({
      verkey: e.detail.key
    })
  }
})